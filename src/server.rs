use crate::{router::Router, service::Service, Result};
use async_std::{
	io::{ReadExt, WriteExt},
	net::{TcpListener, TcpStream, ToSocketAddrs},
	stream::StreamExt,
};
use capnp_rpc::rpc_twoparty_capnp::Side;

#[derive(Default)]
pub struct Server {
	router: Router,
}

impl Server {
	pub fn new() -> Self {
		Self {
			router: Default::default(),
		}
	}

	pub fn add<T: capnp::capability::FromServer<S> + Clone + 'static, S: Clone + 'static>(
		&mut self,
		service: Service<T, S>,
	) -> &mut Self {
		self.router.insert(service.id, Box::new(service));
		self
	}

	pub async fn serve<T: ToSocketAddrs>(&self, addr: T) -> Result<()> {
		let listener = TcpListener::bind(addr).await.expect("");

		let mut incoming = listener.incoming();

		while let Some(stream) = incoming.next().await {
			let stream = stream.expect("");

			handle(self, stream).await.expect("error handling service");
		}

		Ok(())
	}
}

async fn handle(server: &Server, stream: TcpStream) -> Result<()> {
	let (mut reader, mut writer) = (stream.clone(), stream);

	let service_id = service_id(&mut reader).await?;

	if let Some(service) = server.router.get(&service_id) {
		server_ready(&mut writer).await;

		let rpc_system = service.rpc_system(reader, writer, Side::Server);

		println!("inside handler before spawning");

		rpc_system.await.expect("error spawning");
	} else {
		// respond with error service doesn't exist
		todo!()
	}

	Ok(())
}

async fn server_ready(writer: &mut TcpStream) {
	writer.write_all(b"ok").await.expect("");
	writer.flush().await.expect("");
}

async fn service_id(reader: &mut TcpStream) -> Result<u8> {
	let mut buf = vec![0u8; 1024];

	let size = reader
		.read(&mut buf)
		.await
		.expect("error by reading stream at handle::service_id");

	let s = String::from_utf8(buf[..size].to_vec()).unwrap();

	println!("STRING IN service_id {s}");

	let id = s.parse::<u8>().unwrap();

	Ok(id)
}
