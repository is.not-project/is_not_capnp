use crate::service::CapnpService;
use std::collections::HashMap;
use std::ops::{Deref, DerefMut};

#[derive(Default)]
pub struct ServiceMap {
	map: HashMap<u8, Box<dyn CapnpService>>,
}

impl Deref for ServiceMap {
	type Target = HashMap<u8, Box<dyn CapnpService>>;

	fn deref(&self) -> &Self::Target {
		&self.map
	}
}

impl DerefMut for ServiceMap {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.map
	}
}
