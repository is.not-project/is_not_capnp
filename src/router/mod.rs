use crate::service::CapnpService;
use std::collections::HashMap;
use std::ops::{Deref, DerefMut};

mod map;
use map::ServiceMap;

#[derive(Default)]
pub struct Router {
	map: ServiceMap,
}

impl Router {
	pub fn new() -> Self {
		Self {
			map: Default::default(),
		}
	}
}

impl Deref for Router {
	type Target = HashMap<u8, Box<dyn CapnpService>>;

	fn deref(&self) -> &Self::Target {
		&self.map
	}
}

impl DerefMut for Router {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.map
	}
}
