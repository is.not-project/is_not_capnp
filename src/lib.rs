pub type Result<T> = std::result::Result<T, error::Error>;

pub mod client;
pub use client::Client;

pub mod error;
pub use error::Error;

pub mod router;

pub mod server;
pub use server::Server;

pub mod service;
