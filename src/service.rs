use async_std::net::TcpStream;
use capnp::capability::Client;
use capnp_rpc::{rpc_twoparty_capnp::Side, twoparty, RpcSystem};
use std::marker::PhantomData;

#[derive(Clone)]
pub struct Service<T, S>
where
	T: capnp::capability::FromServer<S> + Clone,
	S: Clone,
{
	pub id: u8,
	pub capability: T,
	__: PhantomData<S>,
}

impl<T, S> Service<T, S>
where
	T: capnp::capability::FromServer<S> + Clone,
	S: Clone,
{
	pub fn new(id: u8, capability: T) -> Self {
		Self {
			id,
			capability,
			__: PhantomData,
		}
	}
}

pub trait CapnpService {
	fn rpc_system(&self, reader: TcpStream, writer: TcpStream, side: Side) -> RpcSystem<Side>;
}

impl<T, S> CapnpService for Service<T, S>
where
	T: capnp::capability::FromServer<S> + Clone,
	S: Clone,
{
	fn rpc_system(&self, reader: TcpStream, writer: TcpStream, side: Side) -> RpcSystem<Side> {
		let network = twoparty::VatNetwork::new(reader, writer, side, Default::default());

		let client = if side.eq(&Side::Server) {
			Some(Client::new(self.capability.clone().into_client_hook()))
		} else {
			None
		};

		RpcSystem::new(Box::new(network), client)
	}
}
