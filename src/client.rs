use crate::{
	service::{CapnpService, Service},
	Error, Result,
};
use async_std::{
	io::{ReadExt, WriteExt},
	net::{TcpStream, ToSocketAddrs},
};
use capnp_rpc::{rpc_twoparty_capnp::Side, RpcSystem};

pub struct Client<T, S>
where
	T: capnp::capability::FromServer<S> + Clone,
	S: Clone,
{
	inner: Service<T, S>,
	reader: Option<TcpStream>,
	writer: Option<TcpStream>,
}

impl<T, S> Client<T, S>
where
	T: capnp::capability::FromServer<S> + Clone,
	S: Clone,
{
	pub fn new(inner: Service<T, S>) -> Self {
		Self {
			inner,
			reader: None,
			writer: None,
		}
	}

	pub async fn connect<A: ToSocketAddrs>(&mut self, addr: A) -> Result<T> {
		// pub async fn connect<A: ToSocketAddrs>(&mut self, addr: A) -> Result<Service<T, S>> {
		let stream = TcpStream::connect(addr)
			.await
			.expect("error getting stream");

		let (mut reader, mut writer) = (stream.clone(), stream);

		//BUG:
		//PERFORMANCE: u8 shouldn't be parsed to string before be parsed to bytes
		writer
			.write_all(self.inner.id.to_string().as_bytes())
			.await
			.expect("");

		writer.flush().await.expect("");

		let mut buf = vec![0u8; 1024];

		let size = reader
			.read(&mut buf)
			.await
			.expect("error reading at Client::connect");

		let s = String::from_utf8(buf[..size].to_vec()).unwrap();

		if s.eq("ok") {
			self.reader = Some(reader);
			self.writer = Some(writer);
			println!("oooooook");

			let (rpc_system, t) = self.bootstrap();

			async_std::task::Builder::new().local(rpc_system).expect("");

			Ok(t)
		} else {
			Err(Error::ServerNotReady)
		}
	}

	pub fn rpc_system(&self) -> RpcSystem<Side> {
		assert!(self.reader.is_some());
		assert!(self.writer.is_some());

		self.inner.rpc_system(
			self.reader.clone().expect(""),
			self.writer.clone().expect(""),
			Side::Client,
		)
	}

	pub fn bootstrap(&mut self) -> (RpcSystem<Side>, T) {
		let mut rpc_system = self.rpc_system();
		let t = rpc_system.bootstrap(Side::Server);

		(rpc_system, t)
	}
}
