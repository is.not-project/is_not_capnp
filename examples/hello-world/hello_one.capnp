@0x8b246d21c250872b;

interface HelloOne  {
	struct HelloOneRequest {
		name @0 :Text;
	}

	struct HelloOneReply {
		message @0 :Text;
	}

	sayHelloOne @0 (request: HelloOneRequest) -> (reply: HelloOneReply);
}
