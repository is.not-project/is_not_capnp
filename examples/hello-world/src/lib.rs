pub mod hello_one_capnp {
	include!(concat!(env!("OUT_DIR"), "/hello_one_capnp.rs"));
}

pub mod hello_two_capnp {
	include!(concat!(env!("OUT_DIR"), "/hello_two_capnp.rs"));
}

use capnp::capability::Promise;
use capnp_rpc::pry;
use hello_one_capnp::hello_one;
use hello_two_capnp::hello_two;

pub mod services {
	use super::{hello_one, hello_two, HelloWorldImpl, HelloWorldImpl2};
	use is_not_capnp::service::Service;

	pub fn service_1() -> Service<hello_one::Client, HelloWorldImpl> {
		let hello_world_client: hello_one::Client = capnp_rpc::new_client(HelloWorldImpl);
		Service::<_, HelloWorldImpl>::new(0u8, hello_world_client)
	}

	pub fn service_2() -> Service<hello_two::Client, HelloWorldImpl2> {
		let hello_world_client_2: hello_two::Client = capnp_rpc::new_client(HelloWorldImpl2);
		Service::<_, HelloWorldImpl2>::new(1u8, hello_world_client_2)
	}
}

#[derive(Clone)]
pub struct HelloWorldImpl;

impl hello_one::Server for HelloWorldImpl {
	fn say_hello_one(
		&mut self,
		params: hello_one::SayHelloOneParams,
		mut results: hello_one::SayHelloOneResults,
	) -> Promise<(), ::capnp::Error> {
		let request = pry!(pry!(params.get()).get_request());
		let name = pry!(request.get_name());
		let message = format!("hello service 1, {name}");

		results.get().init_reply().set_message(&message);

		Promise::ok(())
	}
}

#[derive(Clone)]
pub struct HelloWorldImpl2;

impl hello_two::Server for HelloWorldImpl2 {
	fn say_hello_two(
		&mut self,
		params: hello_two::SayHelloTwoParams,
		mut results: hello_two::SayHelloTwoResults,
	) -> capnp::capability::Promise<(), capnp::Error> {
		let request = pry!(pry!(params.get()).get_request());
		let name = pry!(request.get_name());
		let message = format!("hello service 2, {name}");

		results.get().init_reply().set_message(&message);

		Promise::ok(())
	}
}
