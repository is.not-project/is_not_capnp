use hello_world::services;
use is_not_capnp::server::Server;

const ADDR: &str = "127.0.0.1:50650";

#[async_std::main]
async fn main() -> async_std::io::Result<()> {
	Server::new()
		.add(services::service_1())
		.add(services::service_2())
		.serve(ADDR)
		.await
		.expect("error at starting rpc server");

	Ok(())
}
