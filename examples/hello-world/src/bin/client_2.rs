use hello_world::services;
use is_not_capnp::client::Client;

const ADDR: &str = "127.0.0.1:50650";

#[async_std::main]
async fn main() -> async_std::io::Result<()> {
	let mut client = Client::new(services::service_2());

	let client = client.connect(ADDR).await.unwrap();

	let mut request = client.say_hello_two_request();

	request.get().init_request().set_name("hello from client 2");

	let reply = request.send().promise.await.unwrap();

	println!("{:#?}", reply.get().unwrap().get_reply().unwrap());

	Ok(())
}
