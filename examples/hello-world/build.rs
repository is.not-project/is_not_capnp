fn main() -> Result<(), Box<dyn std::error::Error>> {
	capnpc::CompilerCommand::new()
		.file("hello_one.capnp")
		.file("hello_two.capnp")
		.run()
		.unwrap();

	Ok(())
}
