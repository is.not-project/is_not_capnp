@0x8ba74ba4ac1498d8;

interface HelloTwo  {
	struct HelloTwoRequest {
		name @0 :Text;
	}

	struct HelloTwoReply {
		message @0 :Text;
	}

	sayHelloTwo @0 (request: HelloTwoRequest) -> (reply: HelloTwoReply);
}
